'use strict';

const functions = require('firebase-functions');
const nodemailer = require('nodemailer');
const admin = require('firebase-admin');

admin.initializeApp();

// functions.config().gmail.email; //"xolopetapp@gmail.com";
// const gmailPassword =    //
// xxwczntgsysriofv
const mailTransport = nodemailer.createTransport({
    host: 'smtp.gmail.com', port: 465, secure: true, // service: 'Gmail',
    auth: {
        user: 'xolopetapp@gmail.com',
        pass: 'xxwczntgsysriofv' // pass: 'Xolopet1010
    }
});

// firebase deploy --only "functions:sendEmailPedido"
// exports.sendWelcomeEmail = functions.auth.user().onCreate((user) => {
// firebase deploy --only "functions:sendEmailPreOrden"
// firebase deploy --only "functions:fileUploadedBanner"
// https://www.googleapis.com/storage/v1/b/xoloapp-9c3d2.appspot.com/o/banner%2F%C3%ADndice.jpeg


exports.fileUploadedBanner = functions.storage.object().onFinalize(async (object) => {

    const urli = object.mediaLink;
    console.log(url);
    admin.database().ref().child('images_banner').set({'data': "hola"});
    return null;

});


exports.sendEmailPreOrden = functions.database.ref('/preorden/{uid}').onWrite(async (change) => {
    const mailOptions = {
        from: '"XolopetApp" <noreply@firebase.com>',
        to: "xolopetapp@gmail.com",
        subject: 'Nueva PreOrden de Contenedor XolopetApp',
        html: "<h2>Hola, un nuevo usuario solicita un Contenedor.</h2> <h3>Estos son sus datos.</h3> <ul>" + "<li>Nombre: " + change.after._data.nombre + " </li>" + "<li>Correo:  " + change.after._data.correo + " </li>" + "<li>Teléfono: " + change.after._data.telefono + " </li>" + "<li>Raza: " + change.after._data.raza + " </li> " + "<li>Edad:  " + change.after._data.edad + " </li> " + "<li>Marca de Croqueta:  " + change.after._data.marca + " </li>" + "<li>Presentación:  " + change.after._data.presentacion + "</li> </ul>"
    };

    mailTransport.verify(function (error, success) {
        if (error) {
            console.log("ERROR UNO: " + error);
        } else {
            console.log('MAIL ENVIADO');
        }
    });

    try {
        await mailTransport.sendMail(mailOptions);
    } catch (error) {
        console.log("ERRORS 2:" + error);
    }
    return null;
});
//    firebase deploy --only "functions:sendEmailComentario"
exports.sendEmailComentario = functions.database.ref('/comentarios/{uid}').onWrite(async (snap) => {
    var coment = "";
    if (snap.after._data.generales.length >= 2) {
        coment = "<h3>Comentarios generales:</h3><h2>" + snap.after._data.generales + "</h2>";
    }
    const mailOptions = {
        from: '"XolopetApp" <noreply@firebase.com>',
        to: "xolopetapp@gmail.com",
        subject: 'Nuevo comentario de nuestros usuarios XolopetApp',
        html: "<h2>Hola, un nuevo usuario nos brinda estos comentarios.</h2>" + "<h3>¿Qué debemos empezar a hacer?</h3> <h2>" + snap.after._data.empezar + "</h2><h3>¿Qué debemos seguir haciendo?</h3><h2>" + snap.after._data.mantener + "</h2><h3>¿Qué debemos dejar de hacer?</h3><h2>" + snap.after._data.dejar + "<h2>" + coment
    };

    mailTransport.verify(function (error, success) {
        if (error) {
            console.log("ERROR UNO: " + error);
        } else {
            console.log('MAIL ENVIADO');
        }
    });

    try {
        await mailTransport.sendMail(mailOptions);
    } catch (error) {
        console.log("ERRORS 2:" + error);
    }
    return null;
});

exports.sendEmailPedido = functions.database.ref('/pedidos/{uid}').onWrite(async (change) => {
    var men0 = '<h2>' + change.after._data.nombreu + '</h2>';
    var men1 = '<h3>Necesita un pedido</h3>';
    var men2 = ' ';
    var men3 = ' ';

    if (change.after._data.model) {
        men0 = '<p>Uno de los contenedores modelo: <b>' + change.after._data.model + '</b> de <b> ' + change.after._data.nombreu + '</b> se esta quedando vacío.</p></br>';
    }
    if (change.after._data.mascota != " " && change.after._data.mascota != "") {
        men1 = '<p>Necesita un pedido para su mascota <b>' + change.after._data.mascota + '</b>.<br/></p>';
    }
    if (change.after._data.raza != " ") {
        men1 = '<p>Necesita un pedido para su mascota <b>' + change.after._data.mascota + '<br/> de Raza' + change.after._data.raza + '</p>';
    }
    if (change.after._data.pedido != " " && change.after._data.pedido != "") {
        men2 = '<p>Desea Pedir: <b>' + change.after._data.pedido + '</b></p>';
    }
    if (change.after._data.direccion != " ") {
        men3 = '</p><br/><ul>Dirección: <b>' + change.after._data.direccion + '</b></ul>' + '<ul>Teléfono de Contacto: <b>' + change.after._data.telefono + '</b> </ul>' + '<ul>Correo Electronico: <b>' + change.after._data.correo + '</b></ul>'
    }

    const mailOptions = {
        from: '"XolopetApp" <noreply@firebase.com>',
        // to: "xolopetapp@gmail.com",
        to: '<dqniort@gmail.com >',
        subject: 'Solicitud de Pedido XolopetApp',
        html: men0 + men1 + '</br> ' + men2 + men3
    };

    mailTransport.verify(function (error, success) {
        if (error) {
            console.log("ERROR UNO: " + error);
        } else {
            console.log('MAIL ENVIADO');
        }
    });

    try {
        await mailTransport.sendMail(mailOptions);
    } catch (error) {
        console.log("ERRORS 2:" + error);
    }
    return null;

});


//    firebase deploy --only "functions:sendDeviceNotify"
exports.sendDeviceNotify = functions.database.ref('/devices/{devicesUid}').onUpdate(async (snap, context) => {
    if (snap.after._data.pushbattery && snap.after._data.battery <= 0.675) {
        var men = '';
        const getDeviceTokensPromise = admin.database().ref(`/users/${
            snap.after._data.user
        }/tokens`).once('value');
        const getUserPromise = admin.database().ref(`/users/${
            snap.after._data.user
        }`).once('value').then(value => {
            men = "<h2>Hola, un contenedor se esta quedando sin batería.</h2> <ul>" + "<li>Contenedor: " + data.serial + " </li>" + "<li>Modelo: " + data.model + " </li>" + "<li>Porcentaje Bateria: " + Math.round((data.battery * 100) / 4.5) + "% </li>" + "<li>Usuario: " + value.val().nombre + " </li>" + "<li>Email: " + value.val().correo + " </li>";
        });
        const response = await admin.messaging().sendToDevice((await getDeviceTokensPromise).val(), {
            data: {
                'distance': snap.after._data.distance.toString(),
                'mascota': snap.after._data.mascota,
                'model': snap.after._data.model,
                'push': snap.after._data.push.toString(),
                'serial': snap.after._data.serial,
                'user': snap.after._data.user,
                'battery': snap.after._data.battery.toString(),
                'pushbattery': snap.after._data.pushbattery.toString(),
                'payload': 'battery-low'
            },
            notification: {
                title: 'Vuelve Amigo',
                body: 'Uno de tus Dispositivo se está quedando sin batería.',
                sound: 'default',
                click_action: 'FLUTTER_NOTIFICATION_CLICK'
            }
        }).catch(e => {
            console.log("ERROR-PUSH-BATTERY: " + e);
        });

        await admin.database().ref(`/devices/${
            snap.after.key
        }`).update({'pushbattery': false}).then(e => {
            const mailOptions = {
                from: '"XolopetApp" <noreply@firebase.com>',
                to: "xolopetapp@gmail.com",
                subject: 'Un contenedor tiene poca batería.',
                html: men
            };
            try {
                mailTransport.sendMail(mailOptions).then(e => {
                    console.log("CORREO ENVIADO 2:" + error);
                });
            } catch (error) {
                console.log("ERRORS 2:" + error);
            }
            console.log("HOLA MUNDO:");
        }).catch(e => {
            console.log("ERROR5: " + e);
        });
    }


    if (snap.after._data.push == true) {
        const getDeviceTokensPromise = admin.database().ref(`/users/${
            snap.after._data.user
        }/tokens`).once('value');
        var data = {
            'distance': snap.after._data.distance.toString(),
            'mascota': snap.after._data.mascota,
            'model': snap.after._data.model,
            'push': snap.after._data.push.toString(),
            'serial': snap.after._data.serial.toString(),
            'user': snap.after._data.user,
            'payload': 'level-low'
        };
        var _pet = snap.after._data.mascota;
        if (snap.after._data.model == "S" && snap.after._data.distance >= 221) {
            if (snap.after._data.mascota.length <= 1) {
                const payload = {
                    data: data,
                    notification: {
                        title: 'Vuelve Amigo',
                        body: 'Uno de tus Dispositivos se está quedando vacío',
                        sound: 'default',
                        clickAction: 'FLUTTER_NOTIFICATION_CLICK',// content_available: true
                    }
                };

                const response = await admin.messaging().sendToDevice((await getDeviceTokensPromise).val(), payload).catch(e => {
                    console.log("ERROR: " + e);
                });

            } else {
                const payload = {
                    data: data,
                    notification: {
                        title: 'Vuelve Amigo',
                        body: 'El Dispositivo de ' + _pet + ' se está quedando vacío',
                        sound: 'default',
                        click_action: 'FLUTTER_NOTIFICATION_CLICK',
                        // content_available: true
                    }
                };

                const response = await admin.messaging().sendToDevice((await getDeviceTokensPromise).val(), payload).catch(e => {
                    console.log("ERROR: " + e);
                });
            }
        }
        if (snap.after._data.model == "L" && snap.after._data.distance >= 350) {

            if (snap.after._data.mascota.length <= 1) {
                const payload = {
                    data: data,
                    notification: {
                        title: 'Vuelve Amigo',
                        body: 'Uno de tus Dispositivo se está quedando vacío',
                        sound: 'default',
                        click_action: 'FLUTTER_NOTIFICATION_CLICK',
                        // content_available: true
                    }
                };

                const response = await admin.messaging().sendToDevice((await getDeviceTokensPromise).val(), payload).catch(e => {
                    console.log("ERROR: " + e);
                });
            } else {
                const payload = {
                    data: data,
                    notification: {
                        title: 'Vuelve Amigo',
                        body: 'El Dispositivo de ' + _pet + ' se está quedando vacío',
                        sound: 'default',
                        click_action: 'FLUTTER_NOTIFICATION_CLICK',
                        // content_available: "true"
                    }

                };

                const response = await admin.messaging().sendToDevice((await getDeviceTokensPromise).val(), payload).catch(e => {
                    console.log("ERROR: " + e);
                });
            }
        }
    }
});
